﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class Player : MonoBehaviour

{
    //config

    [SerializeField] float runSpeed = 5f;
    [SerializeField] float jumpSpeed = 5f;
    //state

    bool isAlive = true;
         

    //cached component references

    Rigidbody2D myRigidBody;

    Animator myAnimator;

    Collider2D myCollider2D;


   
    // Start is called before the first frame update
    void Start()
    {

        myRigidBody = GetComponent<Rigidbody2D>();

        myAnimator = GetComponent<Animator>();

        myCollider2D = GetComponent<Collider2D>()    ;
    }

    // Update is called once per frame
    void Update()
    {
         
        Run();
        Jump();
        FlipSprite();
    }

    public void FlipSprite()
    {

        bool playerHasHorizontalSpeed = Mathf.Abs(myRigidBody.velocity.x) > Mathf.Epsilon;

        if (playerHasHorizontalSpeed)
        {

            transform.localScale = new Vector2(Mathf.Sign(myRigidBody.velocity.x), 1f);

        }

    }

    public void Run()
    {
        float controlThrow = CrossPlatformInputManager.GetAxis("Horizontal");
        Vector2 playerVelocity = new Vector2(controlThrow * runSpeed, myRigidBody.velocity.y);
        myRigidBody.velocity = playerVelocity;


        bool playerHasHorizontalSpeed = Mathf.Abs(myRigidBody.velocity.x) > Mathf.Epsilon;
        myAnimator.SetBool("Running", playerHasHorizontalSpeed);

    }

    public void Jump()
    {
        if (!myCollider2D.IsTouchingLayers(LayerMask.GetMask("Ground")))
        {
            return; 
        }

        if (CrossPlatformInputManager.GetButtonDown("Jump"))
        {

            Vector2 jumpVelocityToAdd = new Vector2(0f, jumpSpeed);
            myRigidBody.velocity = jumpVelocityToAdd;
        }
        
    }
        

}
