﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumberWizard : MonoBehaviour

{
    int max = 1000;
    int min = 1;
    int guess = 500;

    // Start is called before the first frame update
    void Start()
    {

        StartGame();

    }

    void StartGame()
    {
        max = 1000;
        min = 1;
        guess = 500;

        Debug.Log("Welcome to number wizard");
        Debug.Log("Think of a number between " + min + " and " + max);
        Debug.Log("Press arrow up if your number is higher than my guess");
        Debug.Log("Or Press arrow down if you number is lower than my guess");
        Debug.Log("if your number is right press space");
        Debug.Log("Is your number " + guess + "?");

        max = max + 1;

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("The magical wizard of fucking coding guessed your number");

            StartGame();
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {

            min = guess;
            NextGuess();
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {

            max = guess;
            NextGuess();
        }

        void NextGuess()
        {

            guess = (max + min) / 2;


            Debug.Log("Is " + guess + " the number you are thinking?");
        }
    }
}



